# Sensazonal

[![Build Status](https://www.bitrise.io/app/09299d7edd3ccd10/status.svg?token=xb0q-N6eez9ENU1o1-0SYQ&branch=master)](https://www.bitrise.io/app/09299d7edd3ccd10) [![codebeat badge](https://codebeat.co/badges/0c02c811-6919-49d4-b49e-df54abdb41aa)](https://codebeat.co/projects/gitlab-com-ronanrodrigo-sensazonal-master) [![codecov](https://codecov.io/gl/ronanrodrigo/Sensazonal/branch/master/graph/badge.svg)](https://codecov.io/gl/ronanrodrigo/Sensazonal)


## Requiremetnts
- Xcode 9.30
- Homebrew
- Carthage
- SwiftLint

## Setup Enviroment
- Clone the repository:
`git clone git@gitlab.com:ronanrodrigo/Sensazonal.git`
- Access the project folder and run setup script:
`./setup`

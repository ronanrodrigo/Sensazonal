@testable import Sensazonal

extension FoodViewModel {

    public static var sample: FoodViewModel {
        return FoodViewModel(name: "Name", photo: #imageLiteral(resourceName: "content/strawberry"))
    }

}

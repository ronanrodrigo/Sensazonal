import Foundation

enum SensazonalError: Error {

    case invalidMonth, unknown

}
